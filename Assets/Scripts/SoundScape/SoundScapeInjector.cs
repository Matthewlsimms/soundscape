﻿using SmartestMollusk.Core.Injection;
using SmartestMollusk.Core.Time;
using SmartestMollusk.SoundScape.Time;
using UnityEngine;

namespace SmartestMollusk.SoundScape
{
    [AddComponentMenu("SoundScape/Injector")]
    public class SoundScapeInjector : MonoInjector
    {
        protected override void InitialiseInjectableClasses()
        {
            AddInjectable<IClock>(new SoundScapeClock());
        }
    }
}
