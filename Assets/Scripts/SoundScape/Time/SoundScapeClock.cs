﻿using SmartestMollusk.Core.Time;

namespace SmartestMollusk.SoundScape.Time
{
    public class SoundScapeClock : IClock
    {
        public float RealTimeSinceLastFrame()
        {
            return UnityEngine.Time.deltaTime;
        }

        public float RealTimeSinceStart()
        {
            return UnityEngine.Time.time;
        }

        public float GameTimeSinceLastFrame()
        {
            return UnityEngine.Time.deltaTime;
        }
    }
}
